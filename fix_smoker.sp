#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

#define PLUGIN_VERSION "0.0.1"

public Plugin:myinfo = {
	name = "L4D2 Smoker Fix",
	author = "Osm@n",
	description = "It corrects smoker timer",
	version = PLUGIN_VERSION,
	url = "http://google.com/"
};

public OnPluginStart()
{
	CreateConVar("l4d2_smokerfix",
		PLUGIN_VERSION,
		"L4D2 Smoker Fix",
		FCVAR_PLUGIN|FCVAR_SPONLY|FCVAR_NOTIFY|FCVAR_REPLICATED|FCVAR_DONTRECORD
	);
	HookEvent("ability_use", Event_AbilityUse);
}
public Action:Event_AbilityUse(Handle:event, const String:name[], bool:dontBroadcast)
{
	new String:ability[64];
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	GetEventString(event, "ability", ability, sizeof(ability));
	if(StrEqual("ability_tongue", ability) ){
		ResetInfectedAbility(client, 1.0);
    }
}
stock ResetInfectedAbility(client, Float:time) //this is the function
{
    if (client > 0)
    {
        if (IsClientInGame(client) && IsPlayerAlive(client) && GetClientTeam(client) == 3)
        {
            new ability = GetEntPropEnt(client, Prop_Send, "m_customAbility");
            if (ability > 0)
            {
                SetEntPropFloat(ability, Prop_Send, "m_duration", time);
                SetEntPropFloat(ability, Prop_Send, "m_timestamp", GetGameTime() + time);
            }
        }
    }
}
