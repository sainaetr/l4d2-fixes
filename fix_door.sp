#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

#define PLUGIN_VERSION "0.0.1"

public Plugin:myinfo = {
	name = "L4D2 Door Fix",
	author = "Osm@n",
	description = "it allows admins to lock/unlock the doors",
	version = PLUGIN_VERSION,
	url = "http://google.com/"
};

public OnPluginStart()
{
	CreateConVar("l4d2_doorfix",
		PLUGIN_VERSION,
		"L4D2 Door Fix",
		FCVAR_PLUGIN|FCVAR_SPONLY|FCVAR_NOTIFY|FCVAR_REPLICATED|FCVAR_DONTRECORD
	);
	
	RegAdminCmd("lock", Command_Lock, ADMFLAG_BAN, "lock - locks the doors");
	RegAdminCmd("unlock", Command_Unlock, ADMFLAG_BAN, "unlock - unlocks the doors");
	HookEvent("round_start_post_nav", OnRoundStartPostNav);
	HookEvent("round_end", OnRoundEnd);
}

new DoorEntity = -1;
 
public Action:Command_Lock(client, args)
{
	if(client > 0) {
		if (ToggleDoorLock(true)) {
				PrintToChat(client,"\x01[SM] Saferoom Door \x03Locked");
		}else{
			PrintToChat(client,"\x01[SM] Saferoom Door \x03Could not find");
		}
	}
	return Plugin_Handled;
}
public Action:Command_Unlock(client, args)
{
	if(client > 0) {
		if (ToggleDoorLock(false)) {
				PrintToChat(client,"\x01[SM] Saferoom Door \x03Unlocked");
		}else{
			PrintToChat(client,"\x01[SM] Saferoom Door \x03Could not find");
		}
	}
	return Plugin_Handled;
}
public Action:OnRoundStartPostNav(Handle:event, const String:name[], bool:dontBroadcast)
{
    DoorEntity = GetEntitySafeRoomDoor();
    return Plugin_Continue;
}
public Action:OnRoundEnd(Handle:event, const String:name[], bool:dontBroadcast)
{
    DoorEntity = -1;
    return Plugin_Continue;
}
GetEntitySafeRoomDoor()
{
    new ent = -1;
    while((ent = FindEntityByClassname(ent, "prop_door_rotating_checkpoint")) != -1) {
        if (GetEntProp(ent, Prop_Data, "m_bLocked") == 1) {
            return ent;
        }
    }
    return -1;
}
public bool:ToggleDoorLock(bool:lock)
{
    if(DoorEntity < 0) {
        return false;
    }
    if (lock) {
        SetVariantString("spawnflags 40960");
    }
    else
    {
        SetVariantString("spawnflags 8192");
    }
    AcceptEntityInput(DoorEntity, "AddOutput");
    return true;
}